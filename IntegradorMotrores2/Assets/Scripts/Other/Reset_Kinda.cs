using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset_Kinda : MonoBehaviour
{
    private bool state;
    public GameObject Jumpy;
    public GameObject tp;

    private void Start()
    {
        Jumpy.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            this.gameObject.transform.position = tp.transform.position;
            Jumpy.transform.position = tp.transform.position;
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            state = !state;
            Jumpy.SetActive(state);
        }
    }
}
