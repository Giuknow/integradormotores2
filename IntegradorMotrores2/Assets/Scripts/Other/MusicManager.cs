using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public string RepTrack_name = "";
    public string StopTrack_name = "";
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Heavy") || other.gameObject.CompareTag("Stretch")
            || other.gameObject.CompareTag("Astro") || other.gameObject.CompareTag("Opposite"))
        {
            GestorDeAudio.instancia.detener(StopTrack_name);
            GestorDeAudio.instancia.reproducir(RepTrack_name);

        }
    }

}
