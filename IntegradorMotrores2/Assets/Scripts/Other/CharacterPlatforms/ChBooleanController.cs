using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChBooleanController : MonoBehaviour
{
    public static bool I_Am_Zap;
    public static bool I_Am_Stretch;
    public static bool I_Am_Astro;
    public static bool I_Am_Jumpy;
    public static bool I_Am_Opposite;
    public static bool I_Am_Heavy;
}
