using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeZap : MonoBehaviour
{
    public GameObject Zap;
    public GameObject ZapEmpty;
    public GameObject ZapCursor;
    public GameObject StretchFace;
    public GameObject platformOn;
    public GameObject platformOff;

    public void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E) && (other.gameObject.CompareTag("Heavy") || other.gameObject.CompareTag("Stretch")
            || other.gameObject.CompareTag("Astro") || other.gameObject.CompareTag("Jumpy") || other.gameObject.CompareTag("Opposite")))
        {
            other.gameObject.SetActive(false);
            ZapEmpty.SetActive(true);
            Zap.SetActive(true);
            ZapCursor.SetActive(true);
            ZapCursor.transform.position = this.transform.position;
            Zap.transform.position = this.transform.position;
            ChBooleanController.I_Am_Zap = true;
            GestorDeAudio.instancia.reproducir("ZapAppear");
            StretchFace.SetActive(false);

            ChBooleanController.I_Am_Jumpy = false;
            ChBooleanController.I_Am_Heavy = false;
            ChBooleanController.I_Am_Stretch = false;
            ChBooleanController.I_Am_Astro = false;
            ChBooleanController.I_Am_Opposite = false;

        }
    }
    void Update()
    {
        if (ChBooleanController.I_Am_Zap)
        {
            platformOn.SetActive(false);
            platformOff.SetActive(true);
        }
        else
        {
            platformOn.SetActive(true);
            platformOff.SetActive(false);
        }
    }

    void Start()
    {
        ZapEmpty.SetActive(false);
        platformOff.SetActive(false);
    }
}
