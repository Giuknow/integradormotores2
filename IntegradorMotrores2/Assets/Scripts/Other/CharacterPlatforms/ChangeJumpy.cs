using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeJumpy : MonoBehaviour
{
    public GameObject Jumpy;
    public GameObject JumpyEmpty;
    public GameObject ZapCursor;
    public GameObject StretchFace;
    public GameObject platformOn;
    public GameObject platformOff;

    public void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E) && (other.gameObject.CompareTag("Heavy") || other.gameObject.CompareTag("Stretch")
            || other.gameObject.CompareTag("Astro") || other.gameObject.CompareTag("Zap") || other.gameObject.CompareTag("Opposite")))
        {
            other.gameObject.SetActive(false);
            Jumpy.SetActive(true);
            JumpyEmpty.SetActive(true);
            Jumpy.transform.position = this.transform.position;
            ChBooleanController.I_Am_Jumpy= true;
            ZapCursor.SetActive(false);
            StretchFace.SetActive(false);
            GestorDeAudio.instancia.reproducir("JumpyAppear");
            ChBooleanController.I_Am_Heavy = false;
            ChBooleanController.I_Am_Stretch = false;
            ChBooleanController.I_Am_Astro = false;
            ChBooleanController.I_Am_Zap = false;
            ChBooleanController.I_Am_Opposite = false;
        }
    }
    void Update()
    {
        if (ChBooleanController.I_Am_Jumpy)
        {
            platformOn.SetActive(false);
            platformOff.SetActive(true);
        }
        else
        {
            platformOn.SetActive(true);
            platformOff.SetActive(false);
        }
    }

    void Start()
    {
        JumpyEmpty.SetActive(true);
        platformOff.SetActive(false);
        ChBooleanController.I_Am_Jumpy = true;
    }
}
