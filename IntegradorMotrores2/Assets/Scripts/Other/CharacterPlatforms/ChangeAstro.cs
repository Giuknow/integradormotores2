using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeAstro : MonoBehaviour
{
    public GameObject Astro;
    public GameObject AstroEmpty;
    public GameObject ZapCursor;
    public GameObject StretchFace;
    public GameObject platformOn;
    public GameObject platformOff;
    public void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E) && (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Stretch")
            || other.gameObject.CompareTag("Heavy") || other.gameObject.CompareTag("Zap") || other.gameObject.CompareTag("Opposite")))
        {
            other.gameObject.SetActive(false);
            AstroEmpty.SetActive(true);
            Astro.SetActive(true);
            Astro.transform.position = this.transform.position;
            ChBooleanController.I_Am_Astro = true;
            ZapCursor.SetActive(false);
            StretchFace.SetActive(false);
            GestorDeAudio.instancia.reproducir("AstroAppear");
            ChBooleanController.I_Am_Jumpy = false;
            ChBooleanController.I_Am_Heavy = false;
            ChBooleanController.I_Am_Stretch = false;
            ChBooleanController.I_Am_Zap = false;
            ChBooleanController.I_Am_Opposite = false;
        }
    }
    void Update()
    {
        if (ChBooleanController.I_Am_Astro)
        {
            platformOn.SetActive(false);
            platformOff.SetActive(true);
        } 
        else
        {
            platformOn.SetActive(true);
            platformOff.SetActive(false);
        }
    }
    void Start()
    {
        AstroEmpty.SetActive(false);
        platformOff.SetActive(false);
    }
}

