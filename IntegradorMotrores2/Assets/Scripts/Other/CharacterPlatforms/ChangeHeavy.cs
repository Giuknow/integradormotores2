using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeHeavy : MonoBehaviour
{
    public GameObject Heavy;
    public GameObject HeavyEmpty;
    public GameObject ZapCursor;
    public GameObject StretchFace;
    public GameObject platformOn;
    public GameObject platformOff;

    public void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E) && (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Stretch")
            || other.gameObject.CompareTag("Astro") || other.gameObject.CompareTag("Zap") || other.gameObject.CompareTag("Opposite")))
        {
            other.gameObject.SetActive(false);
            HeavyEmpty.SetActive(true); 
            Heavy.SetActive(true); 
            Heavy.transform.position = this.transform.position;
            ChBooleanController.I_Am_Heavy = true;
            ZapCursor.SetActive(false);
            StretchFace.SetActive(false);
            GestorDeAudio.instancia.reproducir("HeavyAppear");
            ChBooleanController.I_Am_Jumpy = false;
            ChBooleanController.I_Am_Stretch = false;
            ChBooleanController.I_Am_Astro = false;
            ChBooleanController.I_Am_Zap = false;
            ChBooleanController.I_Am_Opposite = false;

        }
    }
    void Update()
    {
        if (ChBooleanController.I_Am_Heavy)
        {
            platformOn.SetActive(false);
            platformOff.SetActive(true);
        }
        else
        {
            platformOn.SetActive(true);
            platformOff.SetActive(false);
        }
    }

    void Start()
    {
        HeavyEmpty.SetActive(false);
        platformOff.SetActive(false);
    }
}
