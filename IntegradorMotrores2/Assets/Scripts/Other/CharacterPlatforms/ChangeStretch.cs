using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeStretch : MonoBehaviour
{
    public GameObject Stretch;
    public GameObject StretchFace;
    public GameObject StretchEmpty;
    public GameObject ZapCursor;
    public GameObject platformOn;
    public GameObject platformOff;

    public void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E) && (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Heavy")
            || other.gameObject.CompareTag("Astro") || other.gameObject.CompareTag("Zap") || other.gameObject.CompareTag("Opposite")))
        {
            Stretch.transform.position = this.transform.position;
            other.gameObject.SetActive(false);
            StretchEmpty.SetActive(true);
            Stretch.SetActive(true);
            StretchFace.SetActive(true);
            ChBooleanController.I_Am_Stretch = true;
            ZapCursor.SetActive(false);
            Stretch.transform.position = this.transform.position;
            GestorDeAudio.instancia.reproducir("StretchAppear");
            ChBooleanController.I_Am_Jumpy = false;
            ChBooleanController.I_Am_Heavy = false;
            ChBooleanController.I_Am_Astro = false;
            ChBooleanController.I_Am_Zap = false;
            ChBooleanController.I_Am_Opposite = false;
        }
    }
    void Update()
    {
        if (ChBooleanController.I_Am_Stretch)
        {
            platformOn.SetActive(false);
            platformOff.SetActive(true);
        }
        else
        {
            platformOn.SetActive(true);
            platformOff.SetActive(false);
        }
    }
    void Start()
    {
        StretchEmpty.SetActive(false);
        platformOff.SetActive(false);
    }
}
