using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeOpposite : MonoBehaviour
{
    public GameObject Opposite;
    public GameObject OppositeEmpty;
    public GameObject ZapCursor;
    public GameObject StretchFace;
    public GameObject platformOn;
    public GameObject platformOff;
    public void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E) && (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Stretch")
            || other.gameObject.CompareTag("Heavy") || other.gameObject.CompareTag("Zap") || other.gameObject.CompareTag("Astro")))
        {
            other.gameObject.SetActive(false);
            OppositeEmpty.SetActive(true);
            OppositeEmpty.SetActive(true);
            Opposite.SetActive(true);
            Opposite.transform.position = this.transform.position;
            ChBooleanController.I_Am_Opposite = true;
            ZapCursor.SetActive(false);
            StretchFace.SetActive(false);
            GestorDeAudio.instancia.reproducir("OppAppear");
            ChBooleanController.I_Am_Jumpy = false;
            ChBooleanController.I_Am_Heavy = false;
            ChBooleanController.I_Am_Stretch = false;
            ChBooleanController.I_Am_Astro = false;
            ChBooleanController.I_Am_Zap = false;

        }
    }
    void Update()
    {
        if (ChBooleanController.I_Am_Opposite)
        {
            platformOn.SetActive(false);
            platformOff.SetActive(true);
        }
        else
        {
            platformOn.SetActive(true);
            platformOff.SetActive(false);
        }

    }
    void Start()
    {
        OppositeEmpty.SetActive(false);
        platformOff.SetActive(false);
    }
}

