using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
   public GameObject tp;
   public GameObject level_bDoor;
   public Animator a;
   private bool active;

    private void Start()
    {
        level_bDoor.SetActive(false);
        active = true;
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Heavy") || other.gameObject.CompareTag("Astro")
            || other.gameObject.CompareTag("Stretch") || other.gameObject.CompareTag("Opposite") && active)
        {
            active = false;
            tp.transform.position = this.transform.position;
            GestorDeAudio.instancia.reproducir("Camera");
            a.Play("DoorClose");
            GameManager.OpenDoor= !GameManager.OpenDoor;
            GameManager.courentLevel++;
            StartCoroutine(BlackDoor());
        }
    }

    public IEnumerator BlackDoor()
    {
        yield return new WaitForSeconds(1f);
        level_bDoor.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
