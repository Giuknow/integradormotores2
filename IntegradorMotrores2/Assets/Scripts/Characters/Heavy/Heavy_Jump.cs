using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heavy_Jump : MonoBehaviour
{
    public Animator animator;
    public Animator camera;
    public static bool estaEnPiso;
    public float alturaSalto = 4f;
    private Rigidbody body;
    void Start()
    {
        body = GetComponent<Rigidbody>();
    }
    void Update()
    {
        Saltar();
    }
    private void Saltar()
    {
        if (Input.GetKeyDown(KeyCode.Space) && estaEnPiso)
        {
            body.AddForce(new Vector2(0.0f, alturaSalto), ForceMode.Impulse);
            estaEnPiso = false;
            GestorDeAudio.instancia.reproducir("HeavyJump");
            animator.Play("JumpHeavy");
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso") == true || collision.gameObject.CompareTag("Heavy") == true)
        {
            estaEnPiso = true;
            animator.Play("HeavyGround");
        }
    }
}
