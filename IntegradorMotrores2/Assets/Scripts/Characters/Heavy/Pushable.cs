using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Pushable : MonoBehaviour
{
    public Rigidbody body;

    private void Start()
    {
        body = GetComponent<Rigidbody>();
        body.mass = 50f;
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Heavy"))
        {
            body.mass = 1f;
        }
        else
        {
            body.mass = 50f;
        }
    }
}
