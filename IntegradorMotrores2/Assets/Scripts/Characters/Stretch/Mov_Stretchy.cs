using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mov_Stretchy : MonoBehaviour
{
    private Rigidbody rb;
    public float rapidez = 3f;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
            float movimientoHorizontal = Input.GetAxis("Horizontal");
            float movimientoVertical = Input.GetAxis("Vertical");
            Debug.Log(movimientoVertical);
            transform.position += Vector3.Normalize(new Vector3(-movimientoVertical, 0, movimientoHorizontal)) * rapidez * Time.deltaTime;
    }
}
