using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stretching : MonoBehaviour
{
    public Animator animator;
    private bool longSize = true;
    void Update()
    {
        Size_Changer();
    }

    private void Size_Changer()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(!longSize)
            {
                GestorDeAudio.instancia.reproducir("Stretch");
                animator.Play("Stretch");
            }
            else
            {
                animator.Play("Reduce");
            }
            longSize = !longSize;
        }
    }
}
