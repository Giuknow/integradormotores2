using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDown : MonoBehaviour
{
    private Rigidbody rb;
    public float fuerzaDeSalto;
    public bool activar = false;
    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }
    void Update()
    {
        ControlDeAccion();
        Accion();
    }
    public void Accion()
    {
        if (activar == true)
        {
            GestorDeAudio.instancia.reproducir("Down");
            rb.AddForce(Vector3.up * fuerzaDeSalto, ForceMode.Impulse);
        }
        else
        {
            GestorDeAudio.instancia.reproducir("Up");
            rb.AddForce(Vector3.down * fuerzaDeSalto, ForceMode.Impulse);
        }
    }
    public void ControlDeAccion()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            activar = !activar;
        }
    }
}
