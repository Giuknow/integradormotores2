using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakCursor : MonoBehaviour
{
    public GameObject Zap;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Piso") || other.gameObject.CompareTag("Room"))
        {
            GestorDeAudio.instancia.reproducir("CursorBreak");
            AvoidWall();
        }
    }
    private void AvoidWall()
    {
        this.gameObject.transform.position = Zap.transform.position;
        this.transform.position += new Vector3(+0, +1, +0);

    }
}
