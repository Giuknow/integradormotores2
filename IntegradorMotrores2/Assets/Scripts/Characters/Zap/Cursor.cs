using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour
{
    private bool axisY;
    private Rigidbody rb;
    public float rapidez = 3f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        axisY = true;
    }
    void Update()
    {
        MovPersonaje();
        SwitchAxis();
    }
    private void MovPersonaje()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Debug.Log(movimientoVertical);
        if (axisY)
        transform.position += Vector3.Normalize(new Vector3(0, movimientoVertical, movimientoHorizontal)) * rapidez * Time.deltaTime;
        if (!axisY)
        transform.position += Vector3.Normalize(new Vector3(-movimientoVertical, 0, movimientoHorizontal)) * rapidez * Time.deltaTime;
    }

    private void SwitchAxis()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            axisY = !axisY;
    }
}

