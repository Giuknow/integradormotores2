using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportZap : MonoBehaviour
{
    public GameObject Cursor;
    private bool wait;
    void Update()
    {
        ZapTP();
    }
    private void ZapTP()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !wait)
        {
            StartCoroutine(ZapCoroutine());
            GestorDeAudio.instancia.reproducir("Zap");
            this.gameObject.transform.position = Cursor.gameObject.transform.position;
        }
    }
    IEnumerator ZapCoroutine()
    {
        wait = true;
        Cursor.SetActive(false);
        yield return new WaitForSeconds(0.4f);
        Cursor.SetActive(true);
        wait = false;
    }
}
