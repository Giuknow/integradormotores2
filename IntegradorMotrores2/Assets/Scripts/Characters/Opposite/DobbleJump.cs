using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DobbleJump : MonoBehaviour
{
    public bool estaEnPiso;
    public float alturaSalto = 5f;
    private Rigidbody body;
    void Start()
    {
        body = GetComponent<Rigidbody>();
    }
    void Update()
    {
        Saltar();
    }
    private void Saltar()
    {
        if (Input.GetKeyDown(KeyCode.Space) && estaEnPiso)
        {
            GestorDeAudio.instancia.reproducir("Flip");
            body.AddForce(new Vector2(0.0f, alturaSalto), ForceMode.Impulse);
            Flip.flip = !Flip.flip;
            estaEnPiso = false;
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso") == true)
        {
            estaEnPiso = true;
        }
    }
}
