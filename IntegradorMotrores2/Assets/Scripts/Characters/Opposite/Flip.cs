using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flip : MonoBehaviour
{
    public static bool flip;
    public GameObject B;
    public GameObject W;

    void Start()
    {
        W.SetActive(false);
    }
    void Update()
    {
        FlipFunction();
    }

    private void FlipFunction()
    {
        if (flip)
        {
            B.SetActive(false);
            W.SetActive(true);
        }
        else
        {
            B.SetActive(true);
            W.SetActive(false);
        }
    }
}
