using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salto : MonoBehaviour
{
    public Animator animator;
    public bool estaEnPiso;
    public float alturaSalto = 5f;
    private Rigidbody body;
    void Start()
    {
        body = GetComponent<Rigidbody>();
    }
    void Update()
    {
        Saltar();
    }
    private void Saltar()
    {
        if (Input.GetKeyDown(KeyCode.Space) && estaEnPiso)
        {
            body.AddForce(new Vector2(0.0f, alturaSalto), ForceMode.Impulse);
            estaEnPiso = false;
            GestorDeAudio.instancia.reproducir("Jump");
            animator.Play("Jumpy_Jump");
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso") == true || collision.gameObject.CompareTag("Heavy") == true)
        {
            estaEnPiso = true;
            animator.Play("Jumpy_Ground");
        }
    }
}
