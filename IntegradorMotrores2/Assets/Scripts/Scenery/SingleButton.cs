using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleButton : MonoBehaviour
{
    private bool _pressed = false;
    public GameObject button;
    private void OnTriggerEnter(Collider other)
    {
        if((other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Heavy") || 
            other.gameObject.CompareTag("Stretch") || other.gameObject.CompareTag("Opposite") 
            || other.gameObject.CompareTag("Astro")) && !_pressed)
        {
            _pressed = true;
            button.SetActive(false);
            GestorDeAudio.instancia.reproducir("Button");
            GameManager.LvlIndex++;
        }
    }
}
