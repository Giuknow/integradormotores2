using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniButton : MonoBehaviour
{
    private bool _pressed = false;
    public GameObject button;
    private void Start()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.CompareTag("Zap") && !_pressed))
        {
            _pressed = true;
            button.SetActive(false);
            GestorDeAudio.instancia.reproducir("Mini");
            Energy.energy++;
        }
    }
}
