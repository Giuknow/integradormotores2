using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZapButton : MonoBehaviour
{
    private bool _pressed = false;
    public GameObject button;
    private void Start()
    {
        button.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.CompareTag("Zap") && !_pressed))
        {
            _pressed = true;
            button.SetActive(true);
            GestorDeAudio.instancia.reproducir("Button");
            this.gameObject.SetActive(false);
        }
    }
}

