using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopOut : MonoBehaviour
{
    public GameObject Wall;
    public static bool pop;
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Stretch") && pop)
        {
            Wall.SetActive(true);
            GestorDeAudio.instancia.reproducir("Pop");
            pop = false;
        }
    }
}
