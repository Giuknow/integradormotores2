using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecAstroButton : MonoBehaviour
{
    public static bool EventIsActive;
    public GameObject b1;
    public GameObject b2;
    public GameObject boff;
    private bool control;
    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Astro"))
        {
            GestorDeAudio.instancia.reproducir("Button");
            GameManager.LvlIndex++;
            this.gameObject.SetActive(false);
        }
    }
    private void Update()
    {
        EventController();
    }
    private void EventController()
    {
        if (EventIsActive)
        StartCoroutine(Time());
    }
    IEnumerator Time()
    {
        if (!control)
        {
            control = true;
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.reproducir("Beep");
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.reproducir("Beep");
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.reproducir("Beep");
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.reproducir("Beep");
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.reproducir("Flip");
            b1.SetActive(true);
            b2.SetActive(false);
            boff.SetActive(true);
            EventIsActive = false;
            control = false;
        }
    }
}
