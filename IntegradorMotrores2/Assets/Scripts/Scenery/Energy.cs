using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : MonoBehaviour
{
    public Animator a;
    public Animator b;
    public Animator c;
    private bool level19 = true;
    private bool level20 = true;
    private bool level23 = true;
    public static int energy = 0;
    void Update()
    {
        Animations();
    }

    private void Animations()
    {
        if (level19 && energy == 1)
        {
            level19 = false;
            a.Play("Lv192");
            GestorDeAudio.instancia.reproducir("Energy");
        }

        if (level20 && energy == 3)
        {
            level20 = false;
            b.Play("Lv201");
            GestorDeAudio.instancia.reproducir("Energy");
        }
        if (level23 && energy == 5)
        {
            level23 = false;
            c.Play("L231");
            GestorDeAudio.instancia.reproducir("Energy");
        }

    }
}
