using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroButton : MonoBehaviour
{
    public GameObject b2;
    public GameObject boff;
    void Start()
    {
        b2.SetActive(false);
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Astro"))
        {
            GestorDeAudio.instancia.reproducir("Button");
            SecAstroButton.EventIsActive = true;
            b2.SetActive(true);
            boff.SetActive(false);
            this.gameObject.SetActive(false);
        }
    }
   
}
