using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyButton : MonoBehaviour
{
    private bool _pressed;
    public GameObject button;

    private void OnTriggerEnter(Collider other)
    {
        if((other.gameObject.CompareTag("Heavy")) && !_pressed && !Heavy_Jump.estaEnPiso)
        {
            _pressed = true;
            button.SetActive(false);
            GameManager.LvlIndex++;
            GestorDeAudio.instancia.reproducir("Button");      
        }
    }
}
