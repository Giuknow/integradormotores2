using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopOn : MonoBehaviour
{
    public GameObject Wall;
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Stretch") && !PopOut.pop)
        {
            Wall.SetActive(false);
            GestorDeAudio.instancia.reproducir("Pop");
            PopOut.pop = true;
        }
    }
}