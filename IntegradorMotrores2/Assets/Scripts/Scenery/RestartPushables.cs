using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartPushables : MonoBehaviour
{
    public Vector3 positionSaved;
    void Start()
    {
        positionSaved = this.gameObject.transform.position;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.R)) 
        {
            this.gameObject.transform.position = positionSaved;
        }
    }
}
