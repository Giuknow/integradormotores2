using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreasurePlate : MonoBehaviour
{
    private bool _pressed;
    public GameObject button;
    public Animator animator;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Heavy") 
            || other.gameObject.CompareTag("Stretch") && !_pressed)
        {
            button.SetActive(false);
            _pressed = true;
            GameManager.LvlIndex++;
            GestorDeAudio.instancia.reproducir("Button");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Heavy") ||
            other.gameObject.CompareTag("Stretch") && _pressed)
        {
            _pressed = false;
            GameManager.LvlIndex--;
            PreasureDoorControl();
            button.SetActive(true);
            
        }
    }

    private void PreasureDoorControl()
    {
        if (GameManager.preasure)
        {
            GameManager.preasure = false;
            animator.Play("DoorClose");
        }
    }
}
