using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumpad : MonoBehaviour
{
    //public Animator animator;
    [Range(100, 1000)]
    public float magnitudDeSalto;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject bouncer = collision.gameObject;
            Rigidbody rb = bouncer.GetComponent<Rigidbody>();
            rb.AddForce(Vector3.up * magnitudDeSalto);
            GestorDeAudio.instancia.reproducir("Boing");
            //animator.Play("Jumpad");
            //StartCoroutine(RebindAnim());
            
        } 
    }

    //IEnumerator RebindAnim()
    //{
    //    yield return new WaitForSeconds(0.5f);
    //    animator.Rebind();
    //}
}


