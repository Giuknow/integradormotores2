using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartController : MonoBehaviour
{
    public Vector3 positionSaved;
    public int controlIndex;
    void Start()
    {
        positionSaved = this.gameObject.transform.position;
    }

    void Update()
    {
        if ((Input.GetKeyUp(KeyCode.R)) && GameManager.courentLevel == controlIndex) 
        {
            this.gameObject.transform.position = positionSaved;
        }
    }
}
